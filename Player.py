class Player:
    def __init__(self, x, y, taille_pixel, taille_serpent):
        self.x = x
        self.y = y
        self.taille_serpent = taille_serpent
        self.taille_pixel = taille_pixel
        self.tab_corps_serpent = []
        self.xmov = 0
        self.ymov = 0
        self.score = 0

    def get_x(self):
        return self.__x

    def get_y(self):
        return self.__y

    def get_taille_pixel(self):
        return self.__taille_pixel

    def get_tab_corps_serpent(self):
        return self.__tab_corps_serpent

    def get_xmov(self):
        return self.__xmov

    def get_ymov(self):
        return self.__ymov

    def get_score(self):
        return self.__score

    def get_taille_serpent(self):
        return self.__taille_serpent

    def set_x(self, x):
        self.__x = x

    def set_y(self, y):
        self.__y = y

    def set_taille_pixel(self, taille_pixel):
        self.__taille_pixel = taille_pixel

    def set_tab_corps_serpent(self, tab_corps_serpent):
        self.__tab_corps_serpent = tab_corps_serpent
        if len(self.__tab_corps_serpent) > self.__taille_serpent:
            del self.__tab_corps_serpent[0]

    def set_xmov(self, xmov):
        self.__xmov = xmov

    def set_ymov(self, ymov):
        self.__ymov = ymov

    def set_score(self, score):
        self.__score = score

    def set_taille_serpent(self, taille_serpent):
        self.__taille_serpent = taille_serpent

    x = property(get_x, set_x)
    y = property(get_y, set_y)
    taille_pixel = property(get_taille_pixel, set_taille_pixel)
    tab_corps_serpent = property(get_tab_corps_serpent, set_tab_corps_serpent)
    taille_serpent = property(get_taille_serpent, set_taille_serpent)
    xmov = property(get_xmov, set_xmov)
    ymov = property(get_ymov, set_ymov)
    score = property(get_score, set_score)

    def bordure_limite(self, largeur, hauteur, grille_torique_actif):
        if grille_torique_actif:
            if self.__x >= largeur:
                self.__x = 0
            elif self.__x <= 0:
                self.__x = largeur
            if self.__y >= hauteur:
                self.__y = 0
            elif self.__y <= 0:
                self.__y = hauteur
            return False
        else:
            if self.__x >= largeur or self.__x <= 0 or self.__y >= hauteur or self.__y <= 0:
                return True
            return False

    def move(self, x, y):
        self.__x += x
        self.__y += y

    def collision_portail(self, portails):
        if self.__x == portails[0][0] and self.__y == portails[0][1]:
            self.__x = portails[1][0]
            self.__y = portails[1][1]
        elif self.__x == portails[1][0] and self.__y == portails[1][1]:
            self.__x = portails[0][0]
            self.__y = portails[0][1]

    def collision_serpent(self, tete, serpent_p2):
        for x in self.__tab_corps_serpent[:-1]:
            if x == tete:
                return True
        for x in serpent_p2:
            if x == tete:
                return True
        return False

    def collision_pomme(self, pomme_x, pomme_y, pomme_type):
        if self.__x == pomme_x and self.__y == pomme_y:
            if pomme_type == "normal":
                self.__taille_serpent += 1
            elif pomme_type == "double":
                self.__taille_serpent += 2
            return True
        return False

    def collision_murs(self, murs):
        for mur in murs:
            if self.__x == mur[0] and self.__y == mur[1]:
                return True
        return False

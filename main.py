import pygame
import time
import random
from Player import Player

pygame.init()

Vert = (26, 255, 0)
Noir = (0, 0, 0)
Rouge = (213, 50, 80)
Gris = (163, 178, 201)
Bleu = (0, 0, 255)
Rose = (255, 0, 255)

Largeur = 800
Hauteur = 800

Grille = pygame.display.set_mode((Largeur, Hauteur))

clock = pygame.time.Clock()

taille_pixel = 20
vitesse = 10
texte = pygame.font.SysFont("arial", 25)


def serpent(taille_pixel, tab_corps_serpent, couleur):
    for x in tab_corps_serpent:
        pygame.draw.rect(Grille, couleur, [x[0], x[1], taille_pixel, taille_pixel])

def generation_mur(): 
    murs = []
    for murAleatoire in range(random.randrange(0, 10)):
        murs.append([round(random.randrange(0, Largeur - taille_pixel) / 20.0) * 20.0, 
        round(random.randrange(0, Hauteur - taille_pixel) / 20.0) * 20.0])
    return murs

def generation_portails():
    portails = []
    for portailAleatoire in range(2):
        portails.append([round(random.randrange(0, Largeur - taille_pixel) / 20.0) * 20.0, 
        round(random.randrange(0, Hauteur - taille_pixel) / 20.0) * 20.0])
    return portails

def generation_pomme():
    pommex = round(random.randrange(0, Largeur - taille_pixel) / 20.0) * 20.0
    pommey = round(random.randrange(0, Hauteur - taille_pixel) / 20.0) * 20.0
    pommespe = ["normal", "double"]
    pommetype = random.choice(pommespe)
    return pommex, pommey, pommetype

def affichage_scores(x, y):
    with open("scores.txt", "r") as fichier:
        for ligne in fichier:
            Grille.blit(texte.render(ligne, True, Noir), [x, y])
            y += 50

def clear_score():
    with open("scores.txt", "w") as fichier:
        fichier.write("")


def gameLoop(first_try = False, murs_actif= True, portails_actif = True, grille_torique_actif = True):
    loop = True
    affichageScore = False
    affichage_option = False
    taille_grille_largeur = Largeur
    taille_grille_hauteur = Hauteur

    pomme = generation_pomme()
    pommex = pomme[0]
    pommey = pomme[1]
    pommetype = "normal"
    # Génère des murs/portails aléatoirement en début de partie
    murs = generation_mur()
    portails = generation_portails()


    player1 = Player((taille_grille_largeur / 2) - (taille_pixel * 2), taille_grille_hauteur / 2, taille_pixel, 1)
    player2 = Player((taille_grille_largeur / 2) + (taille_pixel * 2), taille_grille_hauteur / 2, taille_pixel, 1)
    p1_murs_collision = False
    p2_murs_collision = False
    p1_bordure_collision = False
    p2_bordure_collision = False
    p1_serpent_collision = False
    p2_serpent_collision = False
    while loop:

        while p1_serpent_collision or p1_murs_collision or p1_bordure_collision or p2_serpent_collision or p2_murs_collision or p2_bordure_collision or first_try:
            Grille.fill(Gris)
            if affichage_option == True:
                Grille.blit(texte.render("Appuyez sur B pour revenir au menu", True, Noir), [150, 300])
                Grille.blit(texte.render(f" - M murs activé : {murs_actif}", True, Noir), [150, 350])
                Grille.blit(texte.render(f" - P portails activé : {portails_actif}", True, Noir), [150, 400])
                Grille.blit(texte.render(f" - G grille torique activée : {grille_torique_actif}", True, Noir), [150, 450])
            elif affichageScore == False:
                if first_try == False:
                    Grille.blit(texte.render(f"Joueur {1 if (p2_serpent_collision or p2_murs_collision or p2_bordure_collision) else 2 } à gagné", True, Noir), [150, 300])
                    Grille.blit(texte.render("Player 1, votre score est de:" + str(player1.get_taille_serpent() - 1), True, Noir), [150, 350])
                    Grille.blit(texte.render("Player 2, votre score est de:" + str(player2.get_taille_serpent() - 1), True, Noir), [150, 400])
                Grille.blit(texte.render("Appuyez sur:", True, Noir), [150, 450])
                Grille.blit(texte.render(" - J pour jouer", True, Noir), [150, 500])
                Grille.blit(texte.render(" - S pour sauvegarder votre score", True, Noir), [150, 550])
                Grille.blit(texte.render(" - D pour afficher les scores", True, Noir), [150, 600])
                Grille.blit(texte.render(" - C pour supprimer les scores", True, Noir), [150, 650])
                Grille.blit(texte.render(" - Q pour quitter", True, Noir), [150, 700])
                Grille.blit(texte.render(" - O pour modifier les options", True, Noir), [150, 750])
            else:
                Grille.blit(texte.render("Appuyez sur B pour revenir au menu", True, Noir), [150, 300])
                Grille.blit(texte.render("Listes des scores:", True, Noir), [150, 350])
                affichage_scores(150, 400)
            pygame.display.update()

            #touche a appuyer pour quitter ou relancer
            for input in pygame.event.get():
                if input.type == pygame.KEYDOWN:
                    if input.key == pygame.K_q:
                        loop = False
                        p1_murs_collision = False
                        p2_murs_collision = False
                        p1_serpent_collision = False
                        p2_serpent_collision = False
                    if input.key == pygame.K_j :
                        gameLoop(False, murs_actif, portails_actif, grille_torique_actif)
                    if input.key == pygame.K_s:
                        # Open a file name scores.txt, if the file does not exist create it. Then write the score in the file
                        with open("scores.txt", "a") as file:
                            date = time.strftime("%d/%m/%Y")
                            file.write(f"[{date}] Player 1:" + str(player1.get_taille_serpent() - 1) + "\n")
                            file.write(f"[{date}] Player 2:" + str(player2.get_taille_serpent() - 1) + "\n")
                    if input.key == pygame.K_d:
                        affichageScore = True
                    if input.key == pygame.K_b:
                        affichageScore = False
                        affichage_option = False
                    if input.key == pygame.K_c:
                        clear_score()
                    if input.key == pygame.K_o:
                        affichage_option = True
                    if input.key == pygame.K_m:
                        murs_actif = not murs_actif
                    if input.key == pygame.K_p:
                        portails_actif = not portails_actif
                    if input.key == pygame.K_g:
                        grille_torique_actif = not grille_torique_actif

        #touche a appuyer pour les déplacements
        for input in pygame.event.get():
            if input.type == pygame.QUIT:
                loop = False
                p1_murs_collision = False
                p2_murs_collision = False
                p1_serpent_collision = False
                p2_serpent_collision = False
            if input.type == pygame.KEYDOWN:
                # Player 1 commands
                if input.key == pygame.K_LEFT:
                    player1.set_xmov(-taille_pixel)
                    player1.set_ymov(0)
                elif input.key == pygame.K_RIGHT:
                    player1.set_xmov(taille_pixel)
                    player1.set_ymov(0)
                elif input.key == pygame.K_UP:
                    player1.set_xmov(0)
                    player1.set_ymov(-taille_pixel)
                elif input.key == pygame.K_DOWN:
                    player1.set_xmov(0)
                    player1.set_ymov(taille_pixel)
                # Player 2 commands
                if input.key == pygame.K_q:
                    player2.set_xmov(-taille_pixel)
                    player2.set_ymov(0)
                elif input.key == pygame.K_d:
                    player2.set_xmov(taille_pixel)
                    player2.set_ymov(0)
                elif input.key == pygame.K_z:
                    player2.set_xmov(0)
                    player2.set_ymov(-taille_pixel)
                elif input.key == pygame.K_s:
                    player2.set_xmov(0)
                    player2.set_ymov(taille_pixel)

        p1_bordure_collision = player1.bordure_limite(taille_grille_largeur, taille_grille_hauteur, grille_torique_actif)
        p2_bordure_collision = player2.bordure_limite(taille_grille_largeur, taille_grille_hauteur, grille_torique_actif)

        player1.move(player1.get_xmov(), player1.get_ymov())
        player2.move(player2.get_xmov(), player2.get_ymov())

        Grille.fill(Gris)
        pygame.draw.rect(Grille, Rouge, [pommex, pommey, taille_pixel, taille_pixel])

        if murs_actif:
            for mur in murs:
                # Dessine les murs
                pygame.draw.rect(Grille, Noir, [mur[0], mur[1], taille_pixel, taille_pixel])

            # Collision avec les murs
            p1_murs_collision = player1.collision_murs(murs)
            p2_murs_collision = player2.collision_murs(murs)

        if portails_actif:
            # Transportation avec les portails
            for portail in portails:
                pygame.draw.rect(Grille, Bleu, [portail[0], portail[1], taille_pixel, taille_pixel])
            
            # Collision avec les portails
            player1.collision_portail(portails)
            player2.collision_portail(portails)

        tete_p1 = []
        tete_p1.append(player1.get_x())
        tete_p1.append(player1.get_y())

        tete_p2 = []
        tete_p2.append(player2.get_x())
        tete_p2.append(player2.get_y())

        corp_p1 = player1.get_tab_corps_serpent()
        corp_p2 = player2.get_tab_corps_serpent()

        corp_p1.append(tete_p1)
        corp_p2.append(tete_p2)

        player1.set_tab_corps_serpent(corp_p1)
        player2.set_tab_corps_serpent(corp_p2)

        p1_serpent_collision = player1.collision_serpent(tete_p1, player2.get_tab_corps_serpent())
        p2_serpent_collision = player2.collision_serpent(tete_p2, player1.get_tab_corps_serpent())

        serpent(player1.get_taille_pixel(), player1.get_tab_corps_serpent(), Vert)
        serpent(player2.get_taille_pixel(), player2.get_tab_corps_serpent(), Rose)

        Grille.blit(texte.render("Score P1: " + str(player1.get_taille_serpent() - 1), True, Noir), [0, 0])
        Grille.blit(texte.render("Score P2: " + str(player2.get_taille_serpent() - 1), True, Noir), [0, 25])

        pygame.display.update()

        p1_miam = player1.collision_pomme(pommex, pommey, pommetype)
        p2_miam = player2.collision_pomme(pommex, pommey, pommetype)
        
        if p1_miam or p2_miam:
            # Regénère les murs et portails à chaque fois qu'une pomme est mangée
            pomme = generation_pomme()
            pommex = pomme[0]
            pommey = pomme[1]
            pommetype = pomme[2]
            murs = generation_mur()
            portails = generation_portails()

        clock.tick(vitesse)

    pygame.quit()
    quit()

gameLoop(True)
